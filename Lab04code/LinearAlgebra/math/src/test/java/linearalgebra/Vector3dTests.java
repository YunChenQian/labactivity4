//Yun Chen Qian 2233408
package linearalgebra;

import org.junit.Test;
import static org.junit.Assert.assertEquals;

public class Vector3dTests {
    @Test
    public void getTest(){
        Vector3d aVector = new Vector3d(1, 1, 2);
        assertEquals(1.0, aVector.getX(), 0);
        assertEquals(1.0, aVector.getY(), 0);
        assertEquals(2.0, aVector.getZ(), 0);
    }

    @Test
    public void magTest(){
        Vector3d aVector = new Vector3d(1, 1, 2);
        assertEquals(2.44948974278, aVector.magnitude(), 0.001);
    }

    @Test
    public void dotProductTest(){
        Vector3d aVector = new Vector3d(1, 1, 2);
        Vector3d bVector = new Vector3d(2, 3, 4);
        assertEquals(13, aVector.dotProduct(bVector), 0);
    }

    @Test
    public void addTest(){
        Vector3d aVector = new Vector3d(1, 1, 2);
        Vector3d bVector = new Vector3d(2, 3, 4);
        Vector3d answerVector = new Vector3d(3, 4, 6);
        aVector.add(bVector).equals(answerVector);
    }
}
