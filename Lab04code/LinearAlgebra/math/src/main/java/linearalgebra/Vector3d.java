//Yun Chen Qian 2233408
package linearalgebra;

public class Vector3d {
    private double x;
    private double y;
    private double z;

    public Vector3d(double x, double y, double z){
        this.x = x;
        this.y = y;
        this.z = z;
    }
    
    public double getX(){
        return this.x;
    }

    public double getY(){
        return this.y;
    }

    public double getZ(){
        return this.z;
    }

    public double magnitude(){
        return Math.sqrt(x*x+y*y+z*z);
    }

    public double dotProduct(Vector3d input){
        double dotX = input.getX() + this.getX();
        double dotY = input.getY() + this.getY();
        double dotZ = input.getZ() + this.getZ();
        return dotX+dotY+dotZ;
    }

    public Vector3d add(Vector3d input){
        double dotX = input.getX() + this.getX();
        double dotY = input.getY() + this.getY();
        double dotZ = input.getZ() + this.getZ();
        Vector3d rVector = new Vector3d(dotX, dotY, dotZ);
        return rVector;
    }
}


